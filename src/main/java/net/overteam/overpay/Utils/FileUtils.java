package net.overteam.overpay.Utils;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FileUtils {
    public static ArrayList<String> readFile(File file){
        var output = new ArrayList<String>();
        try(var reader = new Scanner(file)){
            while (reader.hasNextLine()){
                output.add(reader.nextLine());
            }
        }catch (Exception ex){
            System.out.printf("Error occured whilst tried to read file %s%n",file.getAbsolutePath());
        }
        return output;
    }

    public static String readFileToString(File file){
        var output = new StringBuilder();
        var fileArray = readFile(file);
        for(var line : fileArray) output.append(String.format("%s\n", line));
        return output.toString();
    }

    public static boolean writeFile(ArrayList<String> lines, File file){
        var output = true;
        try(var writer = new FileWriter(file)){
            for(var line : lines) writer.append(line);
        } catch (Exception ex) {
            System.out.printf("Error occured whilst tried to write to file %s%n",file.getAbsolutePath());
        }
        return output;
    }

    public static boolean writeFile(String value, File file){
        var output = true;
        try(var writer = new FileWriter(file)){
            writer.append(value);
        } catch (Exception ex) {
            System.out.printf("Error occured whilst tried to write to file %s%n",file.getAbsolutePath());
        }
        return output;
    }
}
