package net.overteam.overpay.Utils;

public class IntUtils {
    public static int parse(String string){
        var output = 0;
        try{
            output = Integer.parseInt(string);
        }catch (Exception ignored){}
        return output;
    }
}
