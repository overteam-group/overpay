package net.overteam.overpay.Utils;

import java.util.function.Consumer;

public class RunnableUtil {
    public RunnableUtil(Consumer consumer){
        ((Runnable) () -> consumer.accept("")).run();
    }
}
