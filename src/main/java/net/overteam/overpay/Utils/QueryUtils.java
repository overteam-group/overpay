package net.overteam.overpay.Utils;

import net.overteam.overpay.Data.QueryParameter;

public class QueryUtils {
    @Deprecated
    public static String where(QueryParameter... parameters){
        var output = new StringBuilder();
        for (var i=0; i<parameters.length; i++){
            var symbol = "'";
            if(!parameters[i].isString()) symbol = "";
            output.append(String.format("%s = %s%s%s",
                    parameters[i].getName(),
                    symbol,
                    parameters[i].getValue(),
                    symbol
            ));
            if(i<parameters.length-2) output.append(" and ");
        }
        return output.toString();
    }
}
