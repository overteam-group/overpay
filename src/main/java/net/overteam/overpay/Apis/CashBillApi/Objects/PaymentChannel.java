package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentChannel {
    @SerializedName(value = "id")
    private String id;

    @SerializedName(value = "availableCurrencies")
    private List<String> availableCurrencies;

    @SerializedName(value = "name")
    private String name;

    @SerializedName(value = "description")
    private String description;

    @SerializedName(value = "logoUrl")
    private String logoUrl;

    public PaymentChannel() {
    }

    public PaymentChannel(String id, List<String> availableCurrencies, String name, String description, String logoUrl) {
        this.id = id;
        this.availableCurrencies = availableCurrencies;
        this.name = name;
        this.description = description;
        this.logoUrl = logoUrl;
    }

    public String getId() {
        return id;
    }

    public PaymentChannel setId(String id) {
        this.id = id;
        return this;
    }

    public List<String> getAvailableCurrencies() {
        return availableCurrencies;
    }

    public PaymentChannel setAvailableCurrencies(List<String> availableCurrencies) {
        this.availableCurrencies = availableCurrencies;
        return this;
    }

    public String getName() {
        return name;
    }

    public PaymentChannel setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PaymentChannel setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public PaymentChannel setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
        return this;
    }
}
