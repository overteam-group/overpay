package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

public class OrderSignData {
    @SerializedName(value = "title")
    private String title;

    @SerializedName(value = "amount")
    private Amount amount;

    @SerializedName(value = "description")
    private String description;

    @SerializedName(value = "additionalData")
    private String additionalData;

    @SerializedName(value = "returnUrl")
    private String returnUrl;

    @SerializedName(value = "negativeReturnUrl")
    private String negativeReturnUrl;

    @SerializedName(value = "paymentChannel")
    private String paymentChannel;

    @SerializedName(value = "languageCode")
    private String languageCode;

    @SerializedName(value = "personalData")
    private PersonalData personalData;

    @SerializedName(value = "referer")
    private String referer;

    @SerializedName(value = "secretPhrase")
    private String secretPhrase;

    public OrderSignData() {
    }

    public OrderSignData(Order order, String secretPhrase){
        this.title = order.getTitle();
        this.amount = order.getAmount();
        this.description = order.getDescription();
        this.additionalData = order.getAdditionalData();
        this.returnUrl = order.getReturnUrl();
        this.negativeReturnUrl = order.getNegativeReturnUrl();
        this.paymentChannel = order.getPaymentChannel();
        this.languageCode = order.getLanguageCode();
        this.personalData = order.getPersonalData();
        this.referer = order.getReferer();
        this.secretPhrase = secretPhrase;
    }

    public OrderSignData(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel, String languageCode, PersonalData personalData, String referer, String secretPhrase) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
        this.languageCode = languageCode;
        this.personalData = personalData;
        this.referer = referer;
        this.secretPhrase = secretPhrase;
    }

    public String getTitle() {
        return title;
    }

    public OrderSignData setTitle(String title) {
        this.title = title;
        return this;
    }

    public Amount getAmount() {
        return amount;
    }

    public OrderSignData setAmount(Amount amount) {
        this.amount = amount;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OrderSignData setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public OrderSignData setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
        return this;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public OrderSignData setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
        return this;
    }

    public String getNegativeReturnUrl() {
        return negativeReturnUrl;
    }

    public OrderSignData setNegativeReturnUrl(String negativeReturnUrl) {
        this.negativeReturnUrl = negativeReturnUrl;
        return this;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public OrderSignData setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
        return this;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public OrderSignData setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public OrderSignData setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
        return this;
    }

    public String getReferer() {
        return referer;
    }

    public OrderSignData setReferer(String referer) {
        this.referer = referer;
        return this;
    }

    public String getSecretPhrase() {
        return secretPhrase;
    }

    public OrderSignData setSecretPhrase(String secretPhrase) {
        this.secretPhrase = secretPhrase;
        return this;
    }
}
