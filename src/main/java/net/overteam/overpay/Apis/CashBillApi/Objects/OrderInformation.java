package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

public class OrderInformation {
    @SerializedName(value = "id")
    private String id;

    @SerializedName(value = "title")
    private String title;

    @SerializedName(value = "status")
    private String status;

    @SerializedName(value = "paymentChannel")
    private String paymentChannel;

    @SerializedName(value = "description")
    private String description;

    @SerializedName(value = "additionalData")
    private String additionalData;

    @SerializedName(value = "amount")
    private Amount amount;

    @SerializedName(value = "registeredAmount")
    private Amount registeredAmount;

    @SerializedName(value = "personalData")
    private PersonalData personalData;

    public OrderInformation() {
    }

    public OrderInformation(String id, String title, String status, String paymentChannel, String description, String additionalData, Amount amount, Amount registeredAmount, PersonalData personalData) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.paymentChannel = paymentChannel;
        this.description = description;
        this.additionalData = additionalData;
        this.amount = amount;
        this.registeredAmount = registeredAmount;
        this.personalData = personalData;
    }

    public String getId() {
        return id;
    }

    public OrderInformation setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public OrderInformation setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public OrderInformation setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public OrderInformation setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OrderInformation setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public OrderInformation setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
        return this;
    }

    public Amount getAmount() {
        return amount;
    }

    public OrderInformation setAmount(Amount amount) {
        this.amount = amount;
        return this;
    }

    public Amount getRegisteredAmount() {
        return registeredAmount;
    }

    public OrderInformation setRegisteredAmount(Amount registeredAmount) {
        this.registeredAmount = registeredAmount;
        return this;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public OrderInformation setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
        return this;
    }
}
