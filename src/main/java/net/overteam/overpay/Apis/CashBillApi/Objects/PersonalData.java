package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

public class PersonalData {
    @SerializedName(value = "firstName")
    private String firstName;

    @SerializedName(value = "surname")
    private String surname;

    @SerializedName(value = "email")
    private String email;

    @SerializedName(value = "country")
    private String country;

    @SerializedName(value = "city")
    private String city;

    @SerializedName(value = "postcode")
    private String postcode;

    @SerializedName(value = "street")
    private String street;

    @SerializedName(value = "house")
    private String house;

    @SerializedName(value = "flat")
    private String flat;

    public PersonalData() {
    }

    public PersonalData(String firstName, String surname, String email, String country, String city, String postcode, String street, String house, String flat) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.country = country;
        this.city = city;
        this.postcode = postcode;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

    public String getFirstName() {
        return firstName;
    }

    public PersonalData setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public PersonalData setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PersonalData setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public PersonalData setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public PersonalData setCity(String city) {
        this.city = city;
        return this;
    }

    public String getPostcode() {
        return postcode;
    }

    public PersonalData setPostcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public PersonalData setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getHouse() {
        return house;
    }

    public PersonalData setHouse(String house) {
        this.house = house;
        return this;
    }

    public String getFlat() {
        return flat;
    }

    public PersonalData setFlat(String flat) {
        this.flat = flat;
        return this;
    }
}
