package net.overteam.overpay.Apis.CashBillApi;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import net.overteam.overpay.Apis.CashBillApi.Objects.Order;
import net.overteam.overpay.Apis.CashBillApi.Objects.OrderInformation;
import net.overteam.overpay.Apis.CashBillApi.Objects.OrderSignData;
import net.overteam.overpay.Apis.CashBillApi.Objects.PaymentChannel;
import net.overteam.overpay.Apis.CashBillApi.Responses.NewPaymentResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static net.overteam.overpay.OverPayApplication.callFunction;

public class CashBillApi {
    @SerializedName(value = "shopId")
    private String shopId;

    @SerializedName(value = "secretPhrase")
    private String secretPhrase;

    @SerializedName(value = "test")
    private boolean test;

    @SerializedName(value = "client")
    private WebClient client;

    public CashBillApi(){
    }

    public CashBillApi(String shopId, String secretPhrase, boolean test) {
        this.shopId = shopId;
        this.secretPhrase = secretPhrase;
        this.test = test;
        this.client = WebClient
                .builder()
                .baseUrl(String.format("https://pay.cashbill.pl/%sws/rest/", (test ? "test" : "") ))
                .build();
    }

    public List<PaymentChannel> getPaymentChannels(){
        var output = new ArrayList<PaymentChannel>();
        try{
            var fetchedChannels = client
                    .get()
                    .uri(String.format("/paymentchannels/%s", shopId))
                    .retrieve()
                    .toEntityList(PaymentChannel.class)
                    .block()
                    .getBody();
            output = new ArrayList<>(fetchedChannels);
        }catch (Exception ignored){}
        return output;
    }

    public NewPaymentResponse createNewPayment(Order order){
        var output = new NewPaymentResponse();
        try{
            String body = (String) callFunction("jsonToUrlEncode", new Gson().toJson(order.setSign(getOrderSign(order))));
            var result = client
                    .post()
                    .uri(String.format("/payment/%s", shopId))
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                    .bodyValue(body)
                    .retrieve()
                    .toEntity(NewPaymentResponse.class)
                    .block()
                    .getBody();
            output = result;
        }catch (Exception ignored){ignored.printStackTrace();}
        return output;
    }

    public OrderInformation fetchOrder(String id){
        var output = new OrderInformation();
        var sign = DigestUtils.sha1Hex(id + getSecretPhrase());
        try{
            var result = client
                    .get()
                    .uri(String.format("payment/%s/%s?sign=%s", shopId, id, sign))
                    .retrieve()
                    .toEntity(OrderInformation.class)
                    .block()
                    .getBody();
            output = result;
        }catch (Exception ignored){ignored.printStackTrace();}
        return output;
    }

    public String getOrderSign(Order order){
        var data = new OrderSignData(order, secretPhrase);
        var input = Objects.requireNonNullElse(data.getTitle(), "") +
                Objects.requireNonNullElse(data.getAmount().getValue(), "") +
                Objects.requireNonNullElse(data.getAmount().getCurrencyCode(), "") +
                Objects.requireNonNullElse(data.getReturnUrl(), "") +
                Objects.requireNonNullElse(data.getDescription(), "") +
                Objects.requireNonNullElse(data.getNegativeReturnUrl(), "") +
                Objects.requireNonNullElse( data.getAdditionalData(), "") +
                Objects.requireNonNullElse(data.getPaymentChannel(), "") +
                Objects.requireNonNullElse(data.getLanguageCode(), "") +
                Objects.requireNonNullElse(data.getReferer(), "");
        if(!Objects.isNull(data.getPersonalData())){
            input += Objects.requireNonNullElse(data.getPersonalData().getFirstName(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getSurname(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getEmail(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getCountry(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getCity(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getPostcode(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getStreet(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getHouse(), "") +
                    Objects.requireNonNullElse(data.getPersonalData().getFlat(), "");
        }
        input += Objects.requireNonNullElse(data.getSecretPhrase(), "");
        var output = DigestUtils.sha1Hex(input);
        return output;
    }

    public String getShopId() {
        return shopId;
    }

    public CashBillApi setShopId(String shopId) {
        this.shopId = shopId;
        return this;
    }

    public String getSecretPhrase() {
        return secretPhrase;
    }

    public CashBillApi setSecretPhrase(String secretPhrase) {
        this.secretPhrase = secretPhrase;
        return this;
    }

    public boolean isTest() {
        return test;
    }

    public CashBillApi setTest(boolean test) {
        this.test = test;
        return this;
    }
}