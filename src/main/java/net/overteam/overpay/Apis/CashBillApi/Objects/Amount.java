package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

public class Amount {
    @SerializedName(value = "value")
    private String value;

    @SerializedName(value = "currencyCode")
    private String currencyCode;

    public Amount() {
    }

    public Amount(String value, String currencyCode) {
        this.value = value;
        this.currencyCode = currencyCode;
    }

    public String getValue() {
        return value;
    }

    public Amount setValue(String value) {
        this.value = value;
        return this;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Amount setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }
}
