package net.overteam.overpay.Apis.CashBillApi.Objects;

import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName(value = "defaultReturnUrl")
    public static String defaultReturnUrl = "";


    @SerializedName(value = "title")
    private String title;

    @SerializedName(value = "amount")
    private Amount amount;

    @SerializedName(value = "description")
    private String description;

    @SerializedName(value = "additionalData")
    private String additionalData;

    @SerializedName(value = "returnUrl")
    private String returnUrl = defaultReturnUrl;

    @SerializedName(value = "negativeReturnUrl")
    private String negativeReturnUrl;

    @SerializedName(value = "paymentChannel")
    private String paymentChannel;

    @SerializedName(value = "languageCode")
    private String languageCode;

    @SerializedName(value = "personalData")
    private PersonalData personalData;

    @SerializedName(value = "referer")
    private String referer;

    @SerializedName(value = "sign")
    private String sign;

    public Order() {
    }

    public Order(String title, Amount amount, String description, String additionalData) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel, String languageCode) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
        this.languageCode = languageCode;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel, String languageCode, PersonalData personalData) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
        this.languageCode = languageCode;
        this.personalData = personalData;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel, String languageCode, PersonalData personalData, String referer) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
        this.languageCode = languageCode;
        this.personalData = personalData;
        this.referer = referer;
    }

    public Order(String title, Amount amount, String description, String additionalData, String returnUrl, String negativeReturnUrl, String paymentChannel, String languageCode, PersonalData personalData, String referer, String sign) {
        this.title = title;
        this.amount = amount;
        this.description = description;
        this.additionalData = additionalData;
        this.returnUrl = returnUrl;
        this.negativeReturnUrl = negativeReturnUrl;
        this.paymentChannel = paymentChannel;
        this.languageCode = languageCode;
        this.personalData = personalData;
        this.referer = referer;
        this.sign = sign;
    }


    public String getTitle() {
        return title;
    }

    public Order setTitle(String title) {
        this.title = title;
        return this;
    }

    public Amount getAmount() {
        return amount;
    }

    public Order setAmount(Amount amount) {
        this.amount = amount;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Order setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public Order setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
        return this;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public Order setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
        return this;
    }

    public String getNegativeReturnUrl() {
        return negativeReturnUrl;
    }

    public Order setNegativeReturnUrl(String negativeReturnUrl) {
        this.negativeReturnUrl = negativeReturnUrl;
        return this;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public Order setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
        return this;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public Order setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public Order setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
        return this;
    }

    public String getReferer() {
        return referer;
    }

    public Order setReferer(String referer) {
        this.referer = referer;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public Order setSign(String sign) {
        this.sign = sign;
        return this;
    }
}
