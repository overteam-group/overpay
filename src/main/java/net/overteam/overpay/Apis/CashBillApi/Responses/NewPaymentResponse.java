package net.overteam.overpay.Apis.CashBillApi.Responses;

import com.google.gson.annotations.SerializedName;

public class NewPaymentResponse {
    @SerializedName(value = "id")
    private String id;

    @SerializedName(value = "redirectUrl")
    private String redirectUrl;

    public NewPaymentResponse() {
    }

    public NewPaymentResponse(String id, String redirectUrl) {
        this.id = id;
        this.redirectUrl = redirectUrl;
    }

    public String getId() {
        return id;
    }

    public NewPaymentResponse setId(String id) {
        this.id = id;
        return this;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public NewPaymentResponse setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
        return this;
    }
}
