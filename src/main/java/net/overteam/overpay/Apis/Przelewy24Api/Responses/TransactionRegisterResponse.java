package net.overteam.overpay.Apis.Przelewy24Api.Responses;

import com.google.gson.annotations.SerializedName;
import net.overteam.overpay.Apis.Przelewy24Api.Objects.TokenContainer;

public class TransactionRegisterResponse {
    @SerializedName(value = "data")
    private TokenContainer data;

    @SerializedName(value = "responseCode")
    private int responseCode;

    public TransactionRegisterResponse(TokenContainer data, int responseCode) {
        this.data = data;
        this.responseCode = responseCode;
    }

    public TransactionRegisterResponse(String data, int responseCode) {
        this.data = new TokenContainer(data);
        this.responseCode = responseCode;
    }

    public TransactionRegisterResponse() {}

    public TokenContainer getData() {
        return data;
    }

    public TransactionRegisterResponse setData(TokenContainer data) {
        this.data = data;
        return this;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public TransactionRegisterResponse setResponseCode(int responseCode) {
        this.responseCode = responseCode;
        return this;
    }
}
