package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class VerificationData {
    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "posId")
    private int posId;

    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "orderId")
    private int orderId;

    @SerializedName(value = "sign")
    private String sign;

    public VerificationData() {}

    public VerificationData(BaseParameters baseParameters, Verification verification, String sign) {
        this.merchantId = baseParameters.getMerchantId();
        this.posId = baseParameters.getPosId();
        this.sessionId = verification.getSessionId();
        this.amount = verification.getAmount();
        this.currency = verification.getCurrency();
        this.orderId = verification.getOrderId();
        this.sign = sign;
    }

    public VerificationData(int merchantId, int posId, String sessionId, int amount, String currency, int orderId, String sign) {
        this.merchantId = merchantId;
        this.posId = posId;
        this.sessionId = sessionId;
        this.amount = amount;
        this.currency = currency;
        this.orderId = orderId;
        this.sign = sign;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public VerificationData setMerchantId(int merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public int getPosId() {
        return posId;
    }

    public VerificationData setPosId(int posId) {
        this.posId = posId;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public VerificationData setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public VerificationData setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public VerificationData setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public int getOrderId() {
        return orderId;
    }

    public VerificationData setOrderId(int orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public VerificationData setSign(String sign) {
        this.sign = sign;
        return this;
    }
}
