package net.overteam.overpay.Apis.Przelewy24Api;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import net.overteam.overpay.Apis.Przelewy24Api.Objects.*;
import net.overteam.overpay.Apis.Przelewy24Api.Responses.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import org.springframework.web.reactive.function.client.WebClient;

public class Przelewy24Api {
    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "posId")
    private int posId;

    @SerializedName(value = "apiKey")
    private String apiKey;

    @SerializedName(value = "crcKey")
    private String crcKey;

    @SerializedName(value = "sandbox")
    private boolean sandbox;

    @SerializedName(value = "baseParameters")
    private BaseParameters baseParameters;
    
    @SerializedName(value = "client")
    private WebClient client;

    public Przelewy24Api(){}
    public Przelewy24Api(int merchantId, String apiKey, String crcKey, boolean sandbox) {
        this(merchantId, merchantId, apiKey, crcKey, sandbox);
    }
    public Przelewy24Api(int merchantId, int posId, String apiKey, String crcKey, boolean sandbox) {
        this.merchantId = merchantId;
        this.posId = posId;
        this.apiKey = apiKey;
        this.crcKey = crcKey;
        this.sandbox = sandbox;
        this.baseParameters = new BaseParameters(this.merchantId, this.posId);
        this.client = WebClient.builder()
                .baseUrl(String.format("https://%s.przelewy24.pl/api/v1", (sandbox ? "sandbox" : "secure")))
                .filter(ExchangeFilterFunctions.basicAuthentication(String.valueOf(posId), apiKey))
                .build();
    }

    public boolean testAccess(){
        var output = true;
        try{
            var result = this.client
                    .get()
                    .uri("/testAccess")
                    .retrieve()
                    .toEntity(TestAccessResponse.class)
                    .block()
                    .getBody();
            output = result.isData();
        }catch (Exception ex){output = false;}
        return output;
    }

    public TransactionResponse createTransaction(Order order){
        var output = new TransactionResponse();
        var hashData = new HashDataCreateTransaction(order.getSessionId(), this.merchantId, order.getAmount(), order.getCurrency(), this.crcKey);
        var sign = DigestUtils.sha384Hex(new Gson().toJson(hashData));
        var orderData = new OrderData(this.baseParameters, order, sign);
        try{
            var result = this.client
                    .post()
                    .uri("/transaction/register")
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(new Gson().toJson(orderData))
                    .retrieve()
                    .toEntity(TransactionRegisterResponse.class)
                    .block()
                    .getBody();
            output.setToken(result.getData().getToken());
            output.setLink(String.format("https://%s.przelewy24.pl/trnRequest/%s", (this.sandbox ? "sandbox" : "secure"), output.getToken()));
        }catch (Exception ignored){ignored.printStackTrace();}
        return output;
    }

    public boolean verifyTransaction(Verification verification){
        var output = true;
        var hashData = new HashDataVerifyTransaction(verification.getSessionId(), verification.getOrderId(), verification.getAmount(), verification.getCurrency(), this.crcKey);
        var sign = DigestUtils.sha384Hex(new Gson().toJson(hashData));
        var verificationData = new VerificationData(this.baseParameters, verification, sign);
        try{
            var result = this.client
                    .put()
                    .uri("/transaction/verify")
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(new Gson().toJson(verificationData))
                    .retrieve()
                    .toEntity(TransactionVerifyResponse.class)
                    .block()
                    .getBody();
            output = result.getData().getStatus().equals("success");
        }catch (Exception ex){
            output = false;
        }
        return output;
    }

    @Deprecated
    public boolean verifyNotification(NotificationRequest notificationRequest){
        var output = true;
        var notificationHash = new HashDataNotification(notificationRequest, this.crcKey);
        var sign = DigestUtils.sha384Hex(new Gson().toJson(notificationHash));
        output = sign.equals(notificationRequest.getSign());
        return output;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public int getPosId() {
        return posId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getCrcKey() {
        return crcKey;
    }

    public boolean isSandbox() {
        return sandbox;
    }

    public BaseParameters getBaseParameters() {
        return baseParameters;
    }

    public WebClient getClient() {
        return client;
    }
}
