package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class OrderData {
    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "posId")
    private int posId;

    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "description")
    private String description;

    @SerializedName(value = "email")
    private String email;

    @SerializedName(value = "country")
    private String country;

    @SerializedName(value = "language")
    private String language;

    @SerializedName(value = "urlReturn")
    private String urlReturn;

    @SerializedName(value = "urlStatus")
    private String urlStatus;

    @SerializedName(value = "timeLimit")
    private int timeLimit;

    @SerializedName(value = "encoding")
    private String encoding;

    @SerializedName(value = "sign")
    private String sign;

    public OrderData(BaseParameters baseParameters, Order order, String sign) {
        this.merchantId = baseParameters.getMerchantId();
        this.posId = baseParameters.getPosId();
        this.sessionId = order.getSessionId();
        this.amount = order.getAmount();
        this.currency = order.getCurrency();
        this.description = order.getDescription();
        this.email = order.getEmail();
        this.country = order.getCountry();
        this.language = order.getLanguage();
        this.urlReturn = order.getUrlReturn();
        this.urlStatus = order.getUrlStatus();
        this.timeLimit = order.getTimeLimit();
        this.encoding = order.getEncoding();
        this.sign = sign;
    }

    public OrderData(int merchantId, int posId, String sessionId, int amount, String currency, String description, String email, String country, String language, String urlReturn, String urlStatus, int timeLimit, String encoding, String sign) {
        this.merchantId = merchantId;
        this.posId = posId;
        this.sessionId = sessionId;
        this.amount = amount;
        this.currency = currency;
        this.description = description;
        this.email = email;
        this.country = country;
        this.language = language;
        this.urlReturn = urlReturn;
        this.urlStatus = urlStatus;
        this.timeLimit = timeLimit;
        this.encoding = encoding;
        this.sign = sign;
    }

    public OrderData() {
    }

    public int getMerchantId() {
        return merchantId;
    }

    public OrderData setMerchantId(int merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public int getPosId() {
        return posId;
    }

    public OrderData setPosId(int posId) {
        this.posId = posId;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public OrderData setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public OrderData setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public OrderData setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OrderData setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public OrderData setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public OrderData setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public OrderData setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getUrlReturn() {
        return urlReturn;
    }

    public OrderData setUrlReturn(String urlReturn) {
        this.urlReturn = urlReturn;
        return this;
    }

    public String getUrlStatus() {
        return urlStatus;
    }

    public OrderData setUrlStatus(String urlStatus) {
        this.urlStatus = urlStatus;
        return this;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public OrderData setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public String getEncoding() {
        return encoding;
    }

    public OrderData setEncoding(String encoding) {
        this.encoding = encoding;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public OrderData setSign(String sign) {
        this.sign = sign;
        return this;
    }
}
