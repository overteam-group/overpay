package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class Verification {
    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "orderId")
    private int orderId;

    public Verification() {
    }

    public Verification(String sessionId, int amount, String currency, int orderId) {
        this.sessionId = sessionId;
        this.amount = amount;
        this.currency = currency;
        this.orderId = orderId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Verification setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public Verification setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public Verification setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public int getOrderId() {
        return orderId;
    }

    public Verification setOrderId(int orderId) {
        this.orderId = orderId;
        return this;
    }
}
