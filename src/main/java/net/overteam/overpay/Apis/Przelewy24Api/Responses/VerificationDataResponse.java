package net.overteam.overpay.Apis.Przelewy24Api.Responses;

import com.google.gson.annotations.SerializedName;

public class VerificationDataResponse {
    @SerializedName(value = "status")
    private String status;

    public VerificationDataResponse() {
    }

    public VerificationDataResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public VerificationDataResponse setStatus(String status) {
        this.status = status;
        return this;
    }
}
