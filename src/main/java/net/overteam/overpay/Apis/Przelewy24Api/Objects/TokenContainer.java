package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class TokenContainer {
    @SerializedName(value = "token")
    private String token;

    public TokenContainer(String token) {
        this.token = token;
    }

    public TokenContainer() {
    }

    public String getToken() {
        return token;
    }

    public TokenContainer setToken(String token) {
        this.token = token;
        return this;
    }
}
