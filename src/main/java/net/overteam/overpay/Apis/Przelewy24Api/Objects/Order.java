package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName(value = "defaultSessionId")
    private static String defaultSessionId = "";

    @SerializedName(value = "defaultAmount")
    private static int defaultAmount = 0;

    @SerializedName(value = "defaultCurrency")
    private static String defaultCurrency = "";

    @SerializedName(value = "defaultDescription")
    private static String defaultDescription = "";

    @SerializedName(value = "defaultEmail")
    private static String defaultEmail = "";

    @SerializedName(value = "defaultCountry")
    private static String defaultCountry = "";

    @SerializedName(value = "defaultLanguage")
    private static String defaultLanguage = "";

    @SerializedName(value = "defaultUrlReturn")
    private static String defaultUrlReturn = "";

    @SerializedName(value = "defaultUrlStatus")
    private static String defaultUrlStatus = "";

    @SerializedName(value = "defaultTimeLimit")
    private static int defaultTimeLimit = 0;

    @SerializedName(value = "defaultEncoding")
    private static String defaultEncoding = "";

    //End of Default Values

    @SerializedName(value = "sessionId")
    private String sessionId = getDefaultSessionId();

    @SerializedName(value = "amount")
    private int amount = getDefaultAmount();

    @SerializedName(value = "currency")
    private String currency = getDefaultCurrency();

    @SerializedName(value = "description")
    private String description = getDefaultDescription();

    @SerializedName(value = "email")
    private String email = getDefaultEmail();

    @SerializedName(value = "country")
    private String country = getDefaultCountry();

    @SerializedName(value = "language")
    private String language = getDefaultLanguage();

    @SerializedName(value = "urlReturn")
    private String urlReturn = getDefaultUrlReturn();

    @SerializedName(value = "urlStatus")
    private String urlStatus = getDefaultUrlStatus();

    @SerializedName(value = "timeLimit")
    private int timeLimit = getDefaultTimeLimit();

    @SerializedName(value = "encoding")
    private String encoding = getDefaultEncoding();

    public Order(){}

    public Order(String sessionId, int amount, String currency, String description, String email, String country, String language, String urlReturn, String urlStatus, int timeLimit, String encoding) {
        this.sessionId = sessionId;
        this.amount = amount;
        this.currency = currency;
        this.description = description;
        this.email = email;
        this.country = country;
        this.language = language;
        this.urlReturn = urlReturn;
        this.urlStatus = urlStatus;
        this.timeLimit = timeLimit;
        this.encoding = encoding;
    }

    public static String getDefaultSessionId() {
        return defaultSessionId;
    }

    public static void setDefaultSessionId(String defaultSessionId) {
        Order.defaultSessionId = defaultSessionId;
    }

    public static int getDefaultAmount() {
        return defaultAmount;
    }

    public static void setDefaultAmount(int defaultAmount) {
        Order.defaultAmount = defaultAmount;
    }

    public static String getDefaultCurrency() {
        return defaultCurrency;
    }

    public static void setDefaultCurrency(String defaultCurrency) {
        Order.defaultCurrency = defaultCurrency;
    }

    public static String getDefaultDescription() {
        return defaultDescription;
    }

    public static void setDefaultDescription(String defaultDescription) {
        Order.defaultDescription = defaultDescription;
    }

    public static String getDefaultEmail() {
        return defaultEmail;
    }

    public static void setDefaultEmail(String defaultEmail) {
        Order.defaultEmail = defaultEmail;
    }

    public static String getDefaultCountry() {
        return defaultCountry;
    }

    public static void setDefaultCountry(String defaultCountry) {
        Order.defaultCountry = defaultCountry;
    }

    public static String getDefaultLanguage() {
        return defaultLanguage;
    }

    public static void setDefaultLanguage(String defaultLanguage) {
        Order.defaultLanguage = defaultLanguage;
    }

    public static String getDefaultUrlReturn() {
        return defaultUrlReturn;
    }

    public static void setDefaultUrlReturn(String defaultUrlReturn) {
        Order.defaultUrlReturn = defaultUrlReturn;
    }

    public static String getDefaultUrlStatus() {
        return defaultUrlStatus;
    }

    public static void setDefaultUrlStatus(String defaultUrlStatus) {
        Order.defaultUrlStatus = defaultUrlStatus;
    }

    public static int getDefaultTimeLimit() {
        return defaultTimeLimit;
    }

    public static void setDefaultTimeLimit(int defaultTimeLimit) {
        Order.defaultTimeLimit = defaultTimeLimit;
    }

    public static String getDefaultEncoding() {
        return defaultEncoding;
    }

    public static void setDefaultEncoding(String defaultEncoding) {
        Order.defaultEncoding = defaultEncoding;
    }


    public String getSessionId() {
        return sessionId;
    }

    public int getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDescription() {
        return description;
    }

    public String getEmail() {
        return email;
    }

    public String getCountry() {
        return country;
    }

    public String getLanguage() {
        return language;
    }

    public String getUrlReturn() {
        return urlReturn;
    }

    public String getUrlStatus() {
        return urlStatus;
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public String getEncoding() {
        return encoding;
    }

    public Order setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public Order setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Order setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public Order setDescription(String description) {
        this.description = description;
        return this;
    }

    public Order setEmail(String email) {
        this.email = email;
        return this;
    }

    public Order setCountry(String country) {
        this.country = country;
        return this;
    }

    public Order setLanguage(String language) {
        this.language = language;
        return this;
    }

    public Order setUrlReturn(String urlReturn) {
        this.urlReturn = urlReturn;
        return this;
    }

    public Order setUrlStatus(String urlStatus) {
        this.urlStatus = urlStatus;
        return this;
    }

    public Order setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public Order setEncoding(String encoding) {
        this.encoding = encoding;
        return this;
    }
}
