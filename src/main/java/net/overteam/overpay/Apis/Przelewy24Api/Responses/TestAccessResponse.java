package net.overteam.overpay.Apis.Przelewy24Api.Responses;

import com.google.gson.annotations.SerializedName;

public class TestAccessResponse {
    @SerializedName(value = "data")
    private boolean data;

    @SerializedName(value = "error")
    private String error;

    public TestAccessResponse() {}
    public TestAccessResponse(boolean data, String error) {
        this.data = data;
        this.error = error;
    }

    public boolean isData() {
        return data;
    }

    public TestAccessResponse setData(boolean data) {
        this.data = data;
        return this;
    }

    public String getError() {
        return error;
    }

    public TestAccessResponse setError(String error) {
        this.error = error;
        return this;
    }
}
