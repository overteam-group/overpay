package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class NotificationRequest {
    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "posId")
    private int posId;

    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "originAmount")
    private int originAmount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "orderId")
    private int orderId;

    @SerializedName(value = "methodId")
    private int methodId;

    @SerializedName(value = "statement")
    private String statement;

    @SerializedName(value = "sign")
    private String sign;

    public NotificationRequest() {
    }

    public NotificationRequest(int merchantId, int posId, String sessionId, int amount, int originAmount, String currency, int orderId, int methodId, String statement, String sign) {
        this.merchantId = merchantId;
        this.posId = posId;
        this.sessionId = sessionId;
        this.amount = amount;
        this.originAmount = originAmount;
        this.currency = currency;
        this.orderId = orderId;
        this.methodId = methodId;
        this.statement = statement;
        this.sign = sign;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public NotificationRequest setMerchantId(int merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public int getPosId() {
        return posId;
    }

    public NotificationRequest setPosId(int posId) {
        this.posId = posId;
        return this;
    }

    public String getSessionId() {
        return sessionId;
    }

    public NotificationRequest setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public NotificationRequest setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public int getOriginAmount() {
        return originAmount;
    }

    public NotificationRequest setOriginAmount(int originAmount) {
        this.originAmount = originAmount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public NotificationRequest setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public int getOrderId() {
        return orderId;
    }

    public NotificationRequest setOrderId(int orderId) {
        this.orderId = orderId;
        return this;
    }

    public int getMethodId() {
        return methodId;
    }

    public NotificationRequest setMethodId(int methodId) {
        this.methodId = methodId;
        return this;
    }

    public String getStatement() {
        return statement;
    }

    public NotificationRequest setStatement(String statement) {
        this.statement = statement;
        return this;
    }

    public String getSign() {
        return sign;
    }

    public NotificationRequest setSign(String sign) {
        this.sign = sign;
        return this;
    }
}
