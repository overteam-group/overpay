package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class StatusContainer {
    @SerializedName(value = "status")
    private String status;

    public StatusContainer() {
    }

    public StatusContainer(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public StatusContainer setStatus(String status) {
        this.status = status;
        return this;
    }
}

