package net.overteam.overpay.Apis.Przelewy24Api.Responses;

import com.google.gson.annotations.SerializedName;

public class TransactionResponse {
    @SerializedName(value = "token")
    private String token;

    @SerializedName(value = "link")
    private String link;

    public TransactionResponse() {}
    public TransactionResponse(String token, String link) {
        this.token = token;
        this.link = link;
    }

    public String getToken() {
        return token;
    }

    public TransactionResponse setToken(String token) {
        this.token = token;
        return this;
    }

    public String getLink() {
        return link;
    }

    public TransactionResponse setLink(String link) {
        this.link = link;
        return this;
    }
}
