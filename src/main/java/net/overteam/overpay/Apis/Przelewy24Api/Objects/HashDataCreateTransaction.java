package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class HashDataCreateTransaction {
    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "crc")
    private String crc;

    public HashDataCreateTransaction() {}

    public HashDataCreateTransaction(String sessionId, int merchantId, int amount, String currency, String crc) {
        this.sessionId = sessionId;
        this.merchantId = merchantId;
        this.amount = amount;
        this.currency = currency;
        this.crc = crc;
    }

    public String getSessionId() {
        return sessionId;
    }

    public HashDataCreateTransaction setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public HashDataCreateTransaction setMerchantId(int merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public HashDataCreateTransaction setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public HashDataCreateTransaction setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getCrc() {
        return crc;
    }

    public HashDataCreateTransaction setCrc(String crc) {
        this.crc = crc;
        return this;
    }
}
