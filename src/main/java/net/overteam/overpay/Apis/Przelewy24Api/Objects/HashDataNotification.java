package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class HashDataNotification {
    @SerializedName(value = "notificationRequest")
    private NotificationRequest notificationRequest;

    @SerializedName(value = "crc")
    private String crc;

    public HashDataNotification() {
    }

    public HashDataNotification(NotificationRequest notificationRequest, String crc) {
        this.notificationRequest = notificationRequest;
        this.crc = crc;
    }

    public NotificationRequest getNotificationRequest() {
        return notificationRequest;
    }

    public HashDataNotification setNotificationRequest(NotificationRequest notificationRequest) {
        this.notificationRequest = notificationRequest;
        return this;
    }

    public String getCrc() {
        return crc;
    }

    public HashDataNotification setCrc(String crc) {
        this.crc = crc;
        return this;
    }
}
