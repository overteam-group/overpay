package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class BaseParameters {
    @SerializedName(value = "merchantId")
    private int merchantId;

    @SerializedName(value = "posId")
    private int posId;

    public BaseParameters(){}
    public BaseParameters(int merchantId, int posId) {
        this.merchantId = merchantId;
        this.posId = posId;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public BaseParameters setMerchantId(int merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public int getPosId() {
        return posId;
    }

    public BaseParameters setPosId(int posId) {
        this.posId = posId;
        return this;
    }
}
