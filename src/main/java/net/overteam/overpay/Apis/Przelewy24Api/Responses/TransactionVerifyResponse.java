package net.overteam.overpay.Apis.Przelewy24Api.Responses;

import com.google.gson.annotations.SerializedName;
import net.overteam.overpay.Apis.Przelewy24Api.Objects.StatusContainer;

public class TransactionVerifyResponse {
    @SerializedName(value = "data")
    private StatusContainer data;

    @SerializedName(value = "responseCode")
    private int responseCode;

    public TransactionVerifyResponse(){}

    public TransactionVerifyResponse(StatusContainer data, int responseCode) {
        this.data = data;
        this.responseCode = responseCode;
    }

    public TransactionVerifyResponse(String data, int responseCode) {
        this.data = new StatusContainer(data);
        this.responseCode = responseCode;
    }

    public StatusContainer getData() {
        return this.data;
    }

    public TransactionVerifyResponse setData(StatusContainer data) {
        this.data = data;
        return this;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public TransactionVerifyResponse setResponseCode(int responseCode) {
        this.responseCode = responseCode;
        return this;
    }
}
