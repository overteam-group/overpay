package net.overteam.overpay.Apis.Przelewy24Api.Objects;

import com.google.gson.annotations.SerializedName;

public class HashDataVerifyTransaction {
    @SerializedName(value = "sessionId")
    private String sessionId;

    @SerializedName(value = "orderId")
    private int orderId;

    @SerializedName(value = "amount")
    private int amount;

    @SerializedName(value = "currency")
    private String currency;

    @SerializedName(value = "crc")
    private String crc;

    public HashDataVerifyTransaction() {}

    public HashDataVerifyTransaction(String sessionId, int orderId, int amount, String currency, String crc) {
        this.sessionId = sessionId;
        this.orderId = orderId;
        this.amount = amount;
        this.currency = currency;
        this.crc = crc;
    }

    public String getSessionId() {
        return sessionId;
    }

    public HashDataVerifyTransaction setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public int getOrderId() {
        return orderId;
    }

    public HashDataVerifyTransaction setOrderId(int orderId) {
        this.orderId = orderId;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public HashDataVerifyTransaction setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public HashDataVerifyTransaction setCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    public String getCrc() {
        return crc;
    }

    public HashDataVerifyTransaction setCrc(String crc) {
        this.crc = crc;
        return this;
    }
}
