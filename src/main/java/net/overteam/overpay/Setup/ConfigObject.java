package net.overteam.overpay.Setup;

import com.google.gson.annotations.SerializedName;

public class ConfigObject{
    @SerializedName(value = "name")
    private String name;

    @SerializedName(value = "jsonName")
    private String jsonName;

    @SerializedName(value = "value")
    private String value;

    @SerializedName(value = "defaultValue")
    private String defaultValue;

    public ConfigObject(){}

    public ConfigObject(String name, String jsonName, String value, String defaultValue){
        this.name = name;
        this.jsonName = jsonName;
        this.value = value;
        this.defaultValue = defaultValue;
    }

    public ConfigObject(String name, String jsonName, String defaultValue){
        this.name = name;
        this.jsonName = jsonName;
        this.defaultValue = defaultValue;
    }

    public ConfigObject(String name, String defaultValue){
        this.name = name;
        this.defaultValue = defaultValue;
    }

    public String getName() {
        return name;
    }

    public ConfigObject setName(String name) {
        this.name = name;
        return this;
    }

    public String getJsonName() {
        return jsonName;
    }

    public ConfigObject setJsonName(String jsonName) {
        this.jsonName = jsonName;
        return this;
    }

    public String getValue() {
        return value;
    }

    public ConfigObject setValue(String value) {
        this.value = value;
        return this;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public ConfigObject setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }
}
