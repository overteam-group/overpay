package net.overteam.overpay.Setup;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static net.overteam.overpay.Utils.FileUtils.*;

public class Setup {
    @SerializedName(value = "configFile")
    private File configFile;

    @SerializedName(value = "configValues")
    private ArrayList<ConfigObject> configValues;

    @SerializedName(value = "quit")
    private boolean quit;

    public Setup(){}
    public Setup(File configFile, boolean quit, ConfigObject... configObjects){
        this.configFile = configFile;
        this.quit = quit;
        this.configValues = new ArrayList<>(Arrays.stream(configObjects).toList());
    }

    public String readConfigFile(){
        return readFileToString(getConfigFile());
    }

    private String ask(String message){
        System.out.print(message);
        return new Scanner(System.in).nextLine();
    }

    private String changeIfEmpty(String value, String defaultValue){
        var output = value;
        if(output.isEmpty() || output==null) output = defaultValue;
        return output;
    }

    public String findAndDecide(String jsonName){
        var object = configValues
                .stream()
                .filter(
                        configObject -> configObject.getJsonName().equals(jsonName)
                )
                .collect(Collectors.toList()).get(0);
        return changeIfEmpty(object.getValue(), object.getDefaultValue());
    }

    public void launch(){
        if(configFile.exists()){
            setupDone();
            return;
        }
        beforeSetup();
        for(var i=0; i<configValues.size(); i++){
            var current = configValues.get(i);
            current.setValue(ask(String.format("%s (%s):", current.getName(), current.getDefaultValue())));
            configValues.set(i,current);
        }
        var config = new HashMap<String,String>();
        configValues.forEach(value ->
                config.put(value.getJsonName(), changeIfEmpty(value.getValue(), value.getDefaultValue()))
        );
        writeFile(new GsonBuilder().setPrettyPrinting().create().toJson(config), configFile);
        afterSetup();
        setupDone();
        if(quit){
            System.out.println("Your configuration was saved, please launch application again.");
            System.exit(0);
        }
    }

    public void beforeSetup(){}

    public void setupDone(){}

    public void afterSetup(){}

    public File getConfigFile() {
        return configFile;
    }

    public Setup setConfigFile(File configFile) {
        this.configFile = configFile;
        return this;
    }

    public ArrayList<ConfigObject> getConfigValues() {
        return configValues;
    }

    public Setup setConfigValues(ArrayList<ConfigObject> configValues) {
        this.configValues = configValues;
        return this;
    }

    public boolean isQuit() {
        return quit;
    }

    public Setup setQuit(boolean quit) {
        this.quit = quit;
        return this;
    }
}
