package net.overteam.overpay.Data;

import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName(value = "server")
    private String server;

    @SerializedName(value = "port")
    private String port;

    @SerializedName(value = "user")
    private String user;

    @SerializedName(value = "password")
    private String password;

    @SerializedName(value = "database")
    private String database;

    public Data(){}
    public Data(String server, String port, String user, String password, String database){
        this.server = server;
        this.port = port;
        this.user = user;
        this.password = password;
        this.database = database;
    }

    public String getServer() {
        return server;
    }

    public Data setServer(String server) {
        this.server = server;
        return this;
    }

    public String getPort() {
        return port;
    }

    public Data setPort(String port) {
        this.port = port;
        return this;
    }

    public String getUser() {
        return user;
    }

    public Data setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Data setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getDatabase() {
        return database;
    }

    public Data setDatabase(String database) {
        this.database = database;
        return this;
    }

    public String getConnectionString(){
        return String.format("jdbc:sqlserver://%s:%s;" +
                "databaseName=%s;" +
                "user=%s;" +
                "password=%s;" +
                "selectMethod=cursor;", server, port, database, user, password);
    }
}
