package net.overteam.overpay.Data;

import com.zaxxer.hikari.HikariDataSource;

class HikariDataSourceBuilder {
    private final HikariDataSource dataSource = new HikariDataSource();

    public HikariDataSourceBuilder(){}

    public HikariDataSourceBuilder setMaximumPoolSize(int size){
        dataSource.setMaximumPoolSize(size);
        return this;
    }

    public HikariDataSourceBuilder setDataSourceClassName(String sourceClassName){
        dataSource.setDataSourceClassName(sourceClassName);
        return this;
    }

    public HikariDataSourceBuilder addDataSourceProperty(String propertyName, String value){
        dataSource.addDataSourceProperty(propertyName,value);
        return this;
    }

    public HikariDataSourceBuilder setPoolName(String poolName){
        dataSource.setPoolName(poolName);
        return this;
    }

    public HikariDataSourceBuilder setMaxLifetime(long maxLifetime){
        dataSource.setMaxLifetime(maxLifetime);
        return this;
    }

    public HikariDataSourceBuilder setMinimumIdle(int minimumIdle){
        dataSource.setMinimumIdle(minimumIdle);
        return this;
    }

    public HikariDataSource build(){
        return dataSource;
    }
}
