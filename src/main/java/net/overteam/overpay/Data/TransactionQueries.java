package net.overteam.overpay.Data;

import net.overteam.overpay.Objects.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TransactionQueries {
    private static final Map<String, String> mappings = Map.of(
            "id","id",
            "date","date",
            "user_id","userId",
            "item_id","itemId",
            "order_id","orderId"
    );

    public static List<Transaction> getAllTransactions(){
        List<Transaction> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var query = con.createQuery("select * from [transaction];");
            output = query.executeAndFetch(Transaction.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<Transaction> getTransactionsById(int id){
        List<Transaction> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [transaction] where %s;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            output = query.executeAndFetch(Transaction.class);
        }catch (Exception ignored){}
        return output;
    }

    public static void addTransaction(Transaction transaction){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "insert into [transaction](date, user_id, item_id, order_id) values(:date, :userId, :itemId, :orderId);";
            var query = con.createQuery(sql).bind(transaction);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }

    public static void deleteTransactionsById(int id){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "delete from [transaction] where %s;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void updateTransaction(Transaction transaction){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "update [transaction] set date=:date, user_id=:userId, item_id=:itemId, order_id=:orderId where id=:id;";
            var query = con.createQuery(sql).bind(transaction);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }
}
