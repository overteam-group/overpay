package net.overteam.overpay.Data;

import net.overteam.overpay.Objects.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserQueries {
    private static final Map<String, String> mappings = Map.of(
            "id","id",
            "nickname","nickname",
            "money","money"
    );

    public static List<User> getAllUsers(){
        List<User> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var query = con.createQuery("select * from [user];");
            output = query.executeAndFetch(User.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<User> getUsersById(int id){
        List<User> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [user] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            output = query.executeAndFetch(User.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<User> getUsersByNickname(String nickname){
        List<User> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [user] where nickname=:nickname;";
            var query = con.createQuery(sql)
                    .addParameter("nickname", nickname);
            output = query.executeAndFetch(User.class);
        }catch (Exception ignored){}
        return output;
    }

    public static void addUser(User user){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "insert into [user](nickname, money) values(:nickname, :money);";
            var query = con.createQuery(sql).bind(user);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void deleteUsersById(int id){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "delete from [user] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void updateUser(User user){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "update [user] set nickname=:nickname, money=:money where id=:id;";
            var query = con.createQuery(sql).bind(user);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }
}
