package net.overteam.overpay.Data;

import javax.sql.DataSource;

import static net.overteam.overpay.OverPayApplication.data;
import static net.overteam.overpay.OverPayApplication.apiConfig;

class HikariWrapper {
    static{
        dataSource = new HikariDataSourceBuilder()
                .setMaximumPoolSize(Integer.parseInt(apiConfig.getPoolSize()))
                .setMinimumIdle(Integer.parseInt(apiConfig.getPoolSize()))
                .setDataSourceClassName("com.microsoft.sqlserver.jdbc.SQLServerDataSource")
                .addDataSourceProperty("url", data.getConnectionString())
                .addDataSourceProperty("user", data.getUser())
                .addDataSourceProperty("password", data.getPassword())
                .setPoolName("hikariCP")
                .setMaxLifetime(240000)
                .build();
    }

    private static DataSource dataSource;

    protected static DataSource get(){
        return dataSource;
    }
}
