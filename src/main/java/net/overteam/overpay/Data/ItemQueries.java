package net.overteam.overpay.Data;

import net.overteam.overpay.Objects.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ItemQueries {
    private static final Map<String, String> mappings = Map.of(
            "id","id",
            "description","description",
            "price","price",
            "category_id","categoryId",
            "archived","archived",
            "type","type",
            "additional_data","additionalData"
    );

    public static List<Item> getAllItems(){
        List<Item> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var query = con.createQuery("select * from [item];");
            output = query.executeAndFetch(Item.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<Item> getItemsById(int id){
        List<Item> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [item] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            output = query.executeAndFetch(Item.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<Item> getItemsByCategoryId(int categoryId){
        List<Item> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [item] where category_id=:categoryId;";
            var query = con.createQuery(sql)
                    .addParameter("categoryId", categoryId);
            output = query.executeAndFetch(Item.class);
        }catch (Exception ignored){}
        return output;
    }

    public static void addItem(Item item){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "insert into [item](name, description, price, category_id, archived, type, additional_data) values(:name, :description, :price, :categoryId, :archived, :type, :additionalData);";
            var query = con.createQuery(sql).bind(item);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void deleteItemsById(int id){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "delete from [item] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void updateItem(Item item){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "update [item] set name=:name, description=:description, price=:price, category_id=:categoryId, archived=:archived, type=:type, additional_data=:additionalData where id=:id;";
            var query = con.createQuery(sql).bind(item);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }
}
