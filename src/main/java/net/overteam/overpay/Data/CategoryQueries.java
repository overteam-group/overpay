package net.overteam.overpay.Data;

import net.overteam.overpay.Objects.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CategoryQueries {
    private static final Map<String, String> mappings = Map.of(
            "id","id",
            "name","name",
            "archived","archived"
    );

    public static List<Category> getAllCategories(){
        List<Category> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var query = con.createQuery("select * from [category];");
            output = query.executeAndFetch(Category.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<Category> getCategoriesById(int id){
        List<Category> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [category] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            output = query.executeAndFetch(Category.class);
        }catch (Exception ignored){}
        return output;
    }

    public static void addCategory(Category category){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "insert into [category](name, archived) values(:name, :archived);";
            var query = con.createQuery(sql).bind(category);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void deleteCategoriesById(int id){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "delete from [category] id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void updateCategory(Category category){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "update [category] set name=:name, archived=:archived where id=:id;";
            var query = con.createQuery(sql).bind(category);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }
}
