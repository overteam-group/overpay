package net.overteam.overpay.Data;

import com.google.gson.annotations.SerializedName;

@Deprecated
public class QueryParameter {
    @SerializedName(value = "name")
    private String name;

    @SerializedName(value = "value")
    private String value;

    @SerializedName(value = "string")
    private boolean string;

    public QueryParameter() {}
    public QueryParameter(String name, String value, boolean string) {
        this.name = name;
        this.value = value;
        this.string = string;
    }

    public String getName() {
        return name;
    }

    public QueryParameter setName(String name) {
        this.name = name;
        return this;
    }

    public String getValue() {
        return value;
    }

    public QueryParameter setValue(String value) {
        this.value = value;
        return this;
    }

    public boolean isString() {
        return string;
    }

    public QueryParameter setString(boolean string) {
        this.string = string;
        return this;
    }
}
