package net.overteam.overpay.Data;

import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.util.Map;

public class Sql2oBuilder {
    private final Sql2o sql2o;

    public Sql2oBuilder(DataSource dataSource){
        sql2o = new Sql2o(dataSource);
    }

    public Sql2oBuilder setDefaultColumnMappings(Map<String, String> defaultColumnMappings){
        this.sql2o.setDefaultColumnMappings(defaultColumnMappings);
        return this;
    }

    public Sql2o build(){
        return this.sql2o;
    }
}
