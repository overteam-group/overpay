package net.overteam.overpay.Data;

import net.overteam.overpay.Objects.Picture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PictureQueries {
    private static final Map<String, String> mappings = Map.of(
            "id","id",
            "user_id","userId",
            "name","name",
            "data","data"
    );

    public static List<Picture> getAllPictures(){
        List<Picture> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var query = con.createQuery("select * from [picture];");
            output = query.executeAndFetch(Picture.class);
        }catch (Exception ignored){}
        return output;
    }

    public static List<Picture> getPicturesById(int id){
        List<Picture> output = new ArrayList<>();
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().open()){
            var sql = "select * from [picture] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            output = query.executeAndFetch(Picture.class);
        }catch (Exception ignored){}
        return output;
    }

    public static void addPicture(Picture picture){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "insert into [picture](user_id, name, data) values(:userId, :name, :data);";
            var query = con.createQuery(sql).bind(picture);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void deletePicturesById(int id){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "delete from [picture] where id=:id;";
            var query = con.createQuery(sql)
                    .addParameter("id", id);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){}
    }

    public static void updatePicture(Picture picture){
        try(var con = new Sql2oBuilder(HikariWrapper.get()).setDefaultColumnMappings(mappings).build().beginTransaction()){
            var sql = "update [picture] set user_id=:userId, name=:name, data=:data where id=:id;";
            var query = con.createQuery(sql).bind(picture);
            query.executeUpdate();
            con.commit();
        }catch (Exception ignored){ignored.printStackTrace();}
    }
}
