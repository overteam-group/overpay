package net.overteam.overpay;

import org.springframework.boot.SpringApplication;

import java.util.Map;
import java.util.Properties;

public class SpringApplicationBuilder {
    private SpringApplication application;

    public SpringApplicationBuilder(Class springApplication){
        this.application = new SpringApplication(springApplication);
    }

    public SpringApplicationBuilder setDefaultProperties(Properties properties){
        this.application.setDefaultProperties(properties);
        return this;
    }

    public SpringApplicationBuilder setDefaultProperties(Map<String, Object> properties){
        this.application.setDefaultProperties(properties);
        return this;
    }

    public SpringApplication build(){
        return this.application;
    }
}
