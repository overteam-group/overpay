package net.overteam.overpay.Controllers;

import net.overteam.overpay.Objects.Item;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static net.overteam.overpay.Data.ItemQueries.*;
import static net.overteam.overpay.Utils.IntUtils.parse;

@CrossOrigin
@RestController
@RequestMapping("/item")
public class ItemController {
    @GetMapping("/get")
    public List<Item> onGet(){
        return getAllItems();
    }

    @GetMapping("/getByCategoryId")
    public List<Item> onGetByCategoryId(@RequestParam(name = "categoryId", required = true) String categoryId){
        return getItemsByCategoryId(parse(categoryId));
    }

    @GetMapping("/getById")
    public Item onGetById(@RequestParam(name = "id", required = true) String id){
        var output = new Item();
        try{
            output = getItemsById(parse(id)).get(0);
        }catch (Exception ignored){}
        return output;
    }
}
