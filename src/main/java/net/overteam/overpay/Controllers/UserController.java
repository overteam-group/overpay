package net.overteam.overpay.Controllers;

import net.overteam.overpay.Objects.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static net.overteam.overpay.Data.UserQueries.*;
import static net.overteam.overpay.Utils.IntUtils.parse;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
    @GetMapping("/get")
    public List<User> onGet(){
        return getAllUsers();
    }

    @GetMapping("/getByNickname")
    public User onGetByNickname(@RequestParam(name = "nickname", required = true) String nickname){
        var output = new User();
        try{
            output = getUsersByNickname(nickname).get(0);
        }catch (Exception ignored){}
        return output;
    }

    @GetMapping("/getById")
    public User onGetById(@RequestParam(name = "id", required = true) String id){
        var output = new User();
        try{
            output = getUsersById(parse(id)).get(0);
        }catch (Exception ignored){}
        return output;
    }
}
