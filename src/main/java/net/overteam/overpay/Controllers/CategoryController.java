package net.overteam.overpay.Controllers;

import net.overteam.overpay.Objects.Category;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static net.overteam.overpay.Data.CategoryQueries.getAllCategories;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {
    @GetMapping("/get")
    public List<Category> onGet(){
        return getAllCategories();
    }
}
