package net.overteam.overpay.Controllers;

import net.overteam.overpay.Objects.MojangApiUuidResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Objects;

@CrossOrigin
@RestController
@RequestMapping("/utils")
public class UtilsController {
    static{
        client = WebClient.builder().build();
    }

    protected static final WebClient client;

    @GetMapping(value = "/fetchPlayerHead", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] onFetchPlayerHead(
            @RequestParam(value = "name", defaultValue = "Steve", required = false) String name,
            @RequestParam(value = "scale", defaultValue = "10", required = false) String scale){
        var output = new byte[0];
        var uuidResponse = new MojangApiUuidResponse();
        try{
            uuidResponse = client
                    .get()
                    .uri(String.format("https://api.mojang.com/users/profiles/minecraft/%s", name))
                    .retrieve()
                    .toEntity(MojangApiUuidResponse.class)
                    .block()
                    .getBody();
            uuidResponse.getId();
        }catch(Exception ignored){
            uuidResponse = new MojangApiUuidResponse("Steve","8667ba71b85a4004af54457a9734eed7");
        }
        try{
            output = client
                    .get()
                    .uri(String.format("https://crafatar.com/renders/head/%s?scale=%s&default=MHF_Steve&overlay", uuidResponse.getId(), scale))
                    .accept(MediaType.ALL)
                    .retrieve()
                    .bodyToMono(byte[].class)
                    .block();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return output;
    }
}