package net.overteam.overpay.Controllers;

import com.google.gson.Gson;
import net.overteam.overpay.Apis.CashBillApi.Objects.Amount;
import net.overteam.overpay.Apis.CashBillApi.Objects.Order;
import net.overteam.overpay.Apis.CashBillApi.Objects.PersonalData;
import net.overteam.overpay.Apis.Przelewy24Api.Responses.TransactionResponse;
import net.overteam.overpay.Objects.NewTransactionRequest;
import net.overteam.overpay.Objects.Transaction;
import net.overteam.overpay.Objects.User;
import net.overteam.overpay.Shop.OrderHandler;
import net.overteam.overpay.Utils.RunnableUtil;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;

import static net.overteam.overpay.Data.ItemQueries.getItemsById;
import static net.overteam.overpay.Data.UserQueries.*;
import static net.overteam.overpay.Data.TransactionQueries.addTransaction;
import static net.overteam.overpay.OverPayApplication.callFunction;
import static net.overteam.overpay.Utils.IntUtils.parse;
import static net.overteam.overpay.OverPayApplication.cb;

@CrossOrigin
@RestController
@RequestMapping("/payment")
public class PaymentController {
    @GetMapping("/after")
    public String onAfter(){
        return "Hello there!";
    }

    @PostMapping("/after2")
    public String onAfter2(){
        return "Hello there!";
    }

    @PostMapping("/newTransaction")
    public TransactionResponse onNewTransaction(@RequestBody String body) {
        var output = new TransactionResponse();
        var request = new Gson().fromJson(body, NewTransactionRequest.class);
        var item = getItemsById(request.getItemId()).get(0);
        var order = new Order()
                .setTitle(String.format("%s-%s-%s", System.currentTimeMillis(), request.setNickname(request.getNickname().replaceAll(
                        "[^a-zA-Z0-9_]+", "")).getNickname(), request.getItemId()))
                .setAmount(new Amount(
                        (String) callFunction("addDotAtSecondFromEnd", String.valueOf(item.getPrice())),
                        "PLN"
                ))
                .setDescription(request.getDescription())
                .setAdditionalData("")
                .setPersonalData(new PersonalData().setEmail(request.getEmail()));
        if(!request.getUrlReturn().isEmpty()) order.setReturnUrl(request.getUrlReturn());
        var newPaymentResponse = cb.createNewPayment(order);
        output = new Gson().fromJson(
                (String) callFunction("convertNewPaymentResponseToTransactionResponse", new Gson().toJson(newPaymentResponse)),
                TransactionResponse.class
        );
        return output;
    }

    @GetMapping("/status")
    public String onStatus(@RequestParam(name = "cmd", defaultValue = "") String cmd,
                           @RequestParam(name = "args", defaultValue = "") String args,
                           @RequestParam(name = "sign", defaultValue = "") String sign){
        var output = "OK";
        var argsList = args.split(",");
        var order = cb.fetchOrder(argsList[0]);
        if(!order.getStatus().equals("PositiveFinish")) return output;

        var params = order.getTitle().split("-");
        var item = getItemsById(parse(params[2])).get(0);
        User user;
        try{
            user = getUsersByNickname(params[1]).get(0);
        }catch (Exception ex){
            addUser(new User(
                    0,
                    params[1],
                    0
            ));
            user = getUsersByNickname(params[1]).get(0);
        }
        var handler = new OrderHandler(user, item);
        new RunnableUtil(consumer -> handler.handle());
        addTransaction(new Transaction(
                0,
                new Timestamp(System.currentTimeMillis()),
                user.getId(),
                item.getId(),
                order.getId()
        ));
        return output;
    }
}
