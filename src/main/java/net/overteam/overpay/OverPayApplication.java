package net.overteam.overpay;

import com.google.gson.Gson;

import net.overteam.overpay.Apis.CashBillApi.CashBillApi;
import net.overteam.overpay.Apis.CashBillApi.Objects.Order;
import net.overteam.overpay.Objects.ApiConfig;
import net.overteam.overpay.Data.Data;
import net.overteam.overpay.Setup.ConfigObject;
import net.overteam.overpay.Setup.Setup;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;
import java.util.Map;
import java.util.stream.Collectors;

import static net.overteam.overpay.Utils.FileUtils.readFileToString;

@SpringBootApplication
public class OverPayApplication {
    public static CashBillApi cb;
	public static ApiConfig apiConfig;
	public static Data data;

	public static ScriptEngine engine = new ScriptEngineManager().getEngineByName("graal.js");
	public static Invocable invocable = (Invocable) engine;

	public static void main(String[] args) {
		new OverPayApplication().loadJavaScriptScripts();

		var databaseSetup = new Setup(new File("database_config.json"), false,
				new ConfigObject("SQL Server Address","server", "localhost"),
				new ConfigObject("Port", "port", "1433"),
				new ConfigObject("User", "user", "DB_USER_NAME"),
				new ConfigObject("Password", "password", "DB_USER_PASSWORD"),
				new ConfigObject("Database", "database", "DB_NAME")){
			@Override
			public void beforeSetup() {
				System.out.println("---Database Setup---");
			}

			@Override
			public void setupDone() {
				var config = new Gson().fromJson(readConfigFile(), Data.class);
				data = new Data(
						config.getServer(),
						config.getPort(),
						config.getUser(),
						config.getPassword(),
						config.getDatabase()
				);
			}
		};

		var cashBillSetup = new Setup(new File("cashbill_config.json"), false,
				new ConfigObject("shopId","shopId","myshop.com"),
				new ConfigObject("secretPhrase","secretPhrase","my-secret-phrase"),
				new ConfigObject("test","test","false")){
			@Override
			public void beforeSetup() {
				System.out.println("---CashBill Setup---");
			}

			@Override
			public void setupDone() {
				var config = new Gson().fromJson(readConfigFile(), CashBillApi.class);
				cb = new CashBillApi(
						config.getShopId(),
						config.getSecretPhrase(),
						config.isTest()
				);
			}
		};

		var cashBillOrderSetup = new Setup(new File("cashbill_order_config.json"), false,
				new ConfigObject("Default Return Url","defaultReturnUrl","https://example.com/afterPayment")){
			@Override
			public void beforeSetup() {
				System.out.println("---CashBill Order Setup---");
			}

			@Override
			public void setupDone() {
				var config = callFunction("fetchValueFromJson","defaultReturnUrl", readConfigFile());
				Order.defaultReturnUrl = (String) config;
			}
		};

		var apiSetup = new Setup(new File("api_config.json"), false,
				new ConfigObject("Login","login","admin"),
				new ConfigObject("Password","password","admin"),
				new ConfigObject("Port","port","50125"),
				new ConfigObject("Connection Pool Size","poolSize","5")){
			@Override
			public void beforeSetup() {
				System.out.println("---API Setup---");
			}

			@Override
			public void setupDone() {
				var config = new Gson().fromJson(readConfigFile(), ApiConfig.class);
				apiConfig = new ApiConfig(
						config.getLogin(),
						config.getPassword(),
						config.getPort(),
						config.getPoolSize()
				);
			}
		};

		databaseSetup.launch();
		apiSetup.launch();
		cashBillSetup.launch();
		cashBillOrderSetup.launch();

		new SpringApplicationBuilder(OverPayApplication.class)
				.setDefaultProperties(
						Map.of(
								"server.port", apiConfig.getPort(),
								"logging.level.root", "DEBUG"
						)
				).build().run(args);
	}

	private void loadJavaScriptScripts() {
		var helper = new Object(){
			public void loadFile(File file) throws ScriptException {
				if(file.isDirectory()){
					loadFile(file);
				}else{
					engine.eval(readFileToString(file));
				}
			}
			public String readResourceAsStream(String path){
				var output = "";
				var inputStream = getClass().getResourceAsStream(path);
				var reader = new BufferedReader(new InputStreamReader(inputStream));
				output = reader.lines().collect(Collectors.joining());
				return output;
			}
		};

		var fileList = new String[]{"/js/utils.js"};
		try{
			for (String s : fileList) {
				engine.eval(helper.readResourceAsStream(s));
			}
		}catch (Exception ex){ex.printStackTrace();}
	}

	public static Object callFunction(String functionName, Object... params){
		var output = new Object();
		try{
			if(params.length<2) output = invocable.invokeFunction(functionName, params[0]);
			else output = invocable.invokeFunction(functionName, params);
		}catch (Exception ignored){}
		return output;
	}
}