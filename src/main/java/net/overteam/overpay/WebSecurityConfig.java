package net.overteam.overpay;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static net.overteam.overpay.OverPayApplication.apiConfig;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS,
                        "/**"
                ).permitAll()
                .antMatchers(HttpMethod.GET,
                        "/payment/after",
                        "/utils/fetchPlayerHead",
                        "/payment/status"
                ).permitAll()
                .antMatchers(HttpMethod.POST,
                        "/payment/after2",
                        "/payment/newTransaction"
                ).permitAll()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        var user =
                User.withDefaultPasswordEncoder()
                        .username(apiConfig.getLogin())
                        .password(apiConfig.getPassword())
                        .roles("USER")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }
}
