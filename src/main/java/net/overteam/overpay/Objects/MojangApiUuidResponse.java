package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class MojangApiUuidResponse {
    @SerializedName(value = "name")
    private String name;

    @SerializedName(value = "id")
    private String id;

    public MojangApiUuidResponse() {
    }

    public MojangApiUuidResponse(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MojangApiUuidResponse setName(String name) {
        this.name = name;
        return this;
    }

    public String getId() {
        return id;
    }

    public MojangApiUuidResponse setId(String id) {
        this.id = id;
        return this;
    }
}
