package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;

public class Transaction {
    @SerializedName(value = "id", alternate = {"Id", "ID"})
    private int id;

    @SerializedName(value = "date", alternate = {"Date", "DATE"})
    private Timestamp date;

    @SerializedName(value = "userId", alternate = {"UserID", "USERID", "UserId", "USER_ID", "user_id", "User_Id", "userid"})
    private int userId;

    @SerializedName(value = "itemId", alternate = {"ItemID", "itemid", "ITEMID", "itemID", "item_id", "item_ID", "Item_ID","Item_Id","ITEM_ID"})
    private int itemId;

    @SerializedName(value = "orderId", alternate = {"OrderID", "orderid", "ORDERID", "orderID", "order_id", "order_ID", "Order_ID","Order_Id","ORDER_ID"})
    private String orderId;

    public Transaction(){}
    public Transaction(int id, Timestamp date, int userId, int itemId, String orderId){
        this.id = id;
        this.date = date;
        this.userId = userId;
        this.itemId = itemId;
        this.orderId = orderId;
    }

    public int getId() {
        return id;
    }

    public Transaction setId(int id) {
        this.id = id;
        return this;
    }

    public Timestamp getDate() {
        return date;
    }

    public Transaction setDate(Timestamp date) {
        this.date = date;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Transaction setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getItemId() {
        return itemId;
    }

    public Transaction setItemId(int itemId) {
        this.itemId = itemId;
        return this;
    }

    public String getOrderId() {
        return orderId;
    }

    public Transaction setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }
}
