package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class NewTransactionRequest {
    @SerializedName(value = "nickname")
    private String nickname;

    @SerializedName(value = "itemId")
    private int itemId;

    @SerializedName(value = "urlReturn")
    private String urlReturn;

    @SerializedName(value = "email")
    private String email;

    @SerializedName(value = "description")
    private String description;

    public NewTransactionRequest() {
    }

    public NewTransactionRequest(String nickname, int itemId, String urlReturn, String email, String description) {
        this.nickname = nickname;
        this.itemId = itemId;
        this.urlReturn = urlReturn;
        this.email = email;
        this.description = description;
    }

    public String getNickname() {
        return nickname;
    }

    public NewTransactionRequest setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public int getItemId() {
        return itemId;
    }

    public NewTransactionRequest setItemId(int itemId) {
        this.itemId = itemId;
        return this;
    }

    public String getUrlReturn() {
        return urlReturn;
    }

    public NewTransactionRequest setUrlReturn(String urlReturn) {
        this.urlReturn = urlReturn;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public NewTransactionRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public NewTransactionRequest setDescription(String description) {
        this.description = description;
        return this;
    }
}