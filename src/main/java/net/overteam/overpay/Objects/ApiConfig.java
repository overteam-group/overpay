package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class ApiConfig {
    @SerializedName(value = "login")
    private String login;

    @SerializedName(value = "password")
    private String password;

    @SerializedName(value = "port")
    private String port;

    @SerializedName(value = "poolSize")
    private String poolSize;

    public ApiConfig() {
    }

    public ApiConfig(String login, String password, String port, String poolSize) {
        this.login = login;
        this.password = password;
        this.port = port;
        this.poolSize = poolSize;
    }

    public String getLogin() {
        return login;
    }

    public ApiConfig setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public ApiConfig setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPort() {
        return port;
    }

    public ApiConfig setPort(String port) {
        this.port = port;
        return this;
    }

    public String getPoolSize() {
        return poolSize;
    }

    public ApiConfig setPoolSize(String poolSize) {
        this.poolSize = poolSize;
        return this;
    }
}
