package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName(value = "id", alternate = {"ID", "Id"})
    private int id;

    @SerializedName(value = "name", alternate = {"Name", "NAME"})
    private String name;

    @SerializedName(value = "description", alternate = {"Description", "DESCRIPTION"})
    private String description;

    @SerializedName(value = "price", alternate = {"Price", "Price"})
    private int price;

    @SerializedName(value = "categoryId", alternate = {"CategoryId", "CATEGORYID", "category_Id", "category_id", "Category_Id", "CATEGORY_ID", "categoryid"})
    private int categoryId;

    @SerializedName(value = "archived", alternate = {"Archived", "ARCHIVED"})
    private boolean archived;

    @SerializedName(value = "type", alternate = {"TYPE", "Type"})
    private String type;

    @SerializedName(value = "additionalData", alternate = {"AdditionalData", "ADDITIONALDATA", "additional_Data", "additional_data", "Additional_Data", "ADDITIONAL_DATA", "additionaldata"})
    private String additionalData;

    public Item(){}
    public Item(int id, String name, String description, int price, int categoryId, boolean archived, String type, String additionalData){
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.categoryId = categoryId;
        this.archived = archived;
        this.type = type;
        this.additionalData = additionalData;
    }

    public int getId() {
        return id;
    }

    public Item setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Item setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Item setPrice(int price) {
        this.price = price;
        return this;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public Item setCategoryId(int categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public boolean isArchived() {
        return archived;
    }

    public Item setArchived(boolean archived) {
        this.archived = archived;
        return this;
    }

    public String getType() {
        return type;
    }

    public Item setType(String type) {
        this.type = type;
        return this;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public Item setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
        return this;
    }
}
