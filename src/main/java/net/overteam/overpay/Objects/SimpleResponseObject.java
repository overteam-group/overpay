package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class SimpleResponseObject {
    @SerializedName(value = "response")
    private String response;

    public SimpleResponseObject() {}

    public SimpleResponseObject(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public SimpleResponseObject setResponse(String response) {
        this.response = response;
        return this;
    }
}
