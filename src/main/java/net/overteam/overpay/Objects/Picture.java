package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class Picture {
    @SerializedName(value = "id", alternate = {"Id", "ID"})
    private int id;

    @SerializedName(value = "userId", alternate = {"USERID", "UserID", "UserId", "user_id", "User_Id", "USER_ID", "user_Id", "User_ID"})
    private int userId;

    @SerializedName(value = "name", alternate = {"Name", "NAME"})
    private String name;

    @SerializedName(value = "data", alternate = {"Data", "DATA"})
    private String data;

    public Picture(){}
    public Picture(int id, int userId, String name, String data){
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public Picture setId(int id) {
        this.id = id;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public Picture setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Picture setName(String name) {
        this.name = name;
        return this;
    }

    public String getData() {
        return data;
    }

    public Picture setData(String data) {
        this.data = data;
        return this;
    }
}
