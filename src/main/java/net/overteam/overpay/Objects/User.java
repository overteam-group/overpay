package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName(value = "id", alternate = {"Id", "ID"})
    private int id;

    @SerializedName(value = "nickname", alternate = {"Nickname", "NICKNAME"})
    private String nickname;

    @SerializedName(value = "money", alternate = {"Money", "MONEY"})
    private int money;

    public User(){}
    public User(String nickname, int money){
        this.nickname = nickname;
        this.money = money;
    }
    public User(int id, String nickname, int money){
        this.id = id;
        this.nickname = nickname;
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public User setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public int getMoney() {
        return money;
    }

    public User setMoney(int money) {
        this.money = money;
        return this;
    }
}
