package net.overteam.overpay.Objects;

import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName(value = "id", alternate = {"ID", "Id"})
    private int id;

    @SerializedName(value = "name", alternate = {"Name","NAME"})
    private String name;

    @SerializedName(value = "archived", alternate = {"Archived","ARCHIVED"})
    private boolean archived;

    public Category(){}
    public Category(int id, String name, boolean archived){
        this.id = id;
        this.name = name;
        this.archived = archived;
    }

    public int getId() {
        return id;
    }

    public Category setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isArchived() {
        return archived;
    }

    public Category setArchived(boolean archived) {
        this.archived = archived;
        return this;
    }
}
