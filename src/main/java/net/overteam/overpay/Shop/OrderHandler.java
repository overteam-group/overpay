package net.overteam.overpay.Shop;

import net.overteam.overpay.Objects.Item;
import net.overteam.overpay.Objects.User;

import static net.overteam.overpay.Data.UserQueries.updateUser;

public class OrderHandler {
    private User user;
    private Item item;

    public OrderHandler() {
    }

    public OrderHandler(User user, Item item) {
        this.user = user;
        this.item = item;
    }

    public void handle(){
        switch(item.getType()){
            default -> { //Money
                user.setMoney(user.getMoney() + Integer.parseInt(item.getAdditionalData()));
                updateUser(user);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public OrderHandler setUser(User user) {
        this.user = user;
        return this;
    }

    public Item getItem() {
        return item;
    }

    public OrderHandler setItem(Item item) {
        this.item = item;
        return this;
    }
}
