const jsonToUrlEncode = (json) => {
    let output = "";
    json = JSON.parse(json);
    let first = true;
    let prefix = "";

    const extract = (object) => {
        for(let key in object){
            if(typeof(object[key])==='object'){
                prefix = key;
                extract(object[key]);
            }else{
                output += `${first ? '' : '&'}${prefix!=="" ? `${prefix}.` : ""}${key}=${encodeURIComponent(object[key])}`;
                first = false;
            }
        }
        prefix = "";
    };

    extract(json);
    return output;
};


const addDotAtSecondFromEnd = (string) => {
    let output = string.split('').reverse().join('').match(/.{1,2}/g);
    output.splice(1, 0, ".");
    return output.join("").split('').reverse().join('');
};


const convertNewPaymentResponseToTransactionResponse = (newPaymentResponse) => {
    let output = JSON.parse(newPaymentResponse);
    output = {token: output['redirectUrl'].split('/').reverse()[0], link: output['redirectUrl']};
    return JSON.stringify(output);
};


const fetchValueFromJson = (valueName, json) => {
    return JSON.parse(json)[valueName];
};




print("JS Utils File loaded!");